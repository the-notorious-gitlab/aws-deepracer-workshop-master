# AWS-DeepRacer-Workshop

## AWS Deepracer
<p align="">
    <img src="images/deepracer.png" width="30%" height="30%">
</p>    

AWS DeepRacer is a 1/18th scale race car which gives you an interesting and fun way to get started with reinforcement learning (RL). RL is an advanced machine learning (ML) technique which takes a very different approach to training models than other machine learning methods. Its super power is that it learns very complex behaviors without requiring any labeled training data, and can make short term decisions while optimizing for a longer term goal.


## What are inside AWS Deepracer

### Robot Operating System

<p align="">
    <img src="images/ros_logo.png" width="35%" height="35%">
</p>  

**Robot Operating System (ROS)** is a middleware designed for robots, which has been globally prevalent for robotic development. **Deepracer** is installed with ROS, which makes it highly compatible with many applications. In lab1 it will be fully introduced.

### AWS Platform

<p align="">
    <img src="images/aws_logo.png" width="35%" height="35%">
</p>  

**Amazon Web Services (AWS)** is the world’s most comprehensive and broadly adopted cloud platform. For robotic development, AWS has **SageMaker** to train model, and **Robomaker** to develop robot application and simulation. As for **IoT Deployment**, AWS has **Greengrass** to deal with it. 

### Machine Learning
Since supervised learning is greatly developed, especially deep learning, it has been appeared that machine learning becomes the most crucial part of robots. However, as the saying goes, there is always a bottleneck. Supervised learning faces serveral problems, especially requiring massive labeled training data, which makes it harder to improve models. Fortunately, reinforcement learning(RL) appears to solve this problem. Without requiring labeled training data, RL can learn all by itself, which means, it has larger improvement than supervised learning does.


<p align="">
    <img src="images/chart.png" width="60%" height="60%">
</p>   


## Lab
- [Lab1 - ROS Setup TurtleSlim](./Lab1-ROS-Setup-TurtleSim/README.md)
- [Lab2 - Training using Amazon SageMaker and AWS RoboMaker](./Lab2-Training-using-Amazon-SageMaker-and-AWS-RoboMaker/README.md)
- [Lab3 - Deploy and tuning model with DeepRacer](./Lab3-Deploy-and-tuning-model-with-DeepRacer/README.md)
