# Lab1-ROS-Setup-TurtleSlim
## ROS Introduction

<p align="">
    <img src="images/ros.png" width="70%" height="70%">
</p>    
For a long time, robotics development faces an issue that different robot hardware requires software to rewrite, even the function are all the same. Robot Operating System(ROS) appeared to solve such problem. ROS is a middleware, between operating system & robot applications. It provides consistent API which can be used by robot applications. Therefore, as long as the hardware platform is installed with ROS, there's no need to rewrite software, which means, one software for any hardware.

### Mainstream version
#### ROS kinetic kame - ubuntu 16.04 LTS
<p align="">
    <img src="images/kinetic_kame.png" width="90%" height="90%">
    </p>

## Before We Start
You'll need an **Ubuntu** desktop to run ROS. It can be a notebook with disk partition installed with ubuntu, or a SBC like Raspberry Pi, NVIDIA Jetson NANO, and so on.
 <br>
 If not, you can either use virtual machine or cloud services such as **AWS EC2**.
Here we select **AWS EC2** due to its best convenience and durability.


## Setting Up AWS EC2

<p align="">
    <img src="images/aws_logo.png" width="40%" height="40%">
    </p>

### Launch an instance

- Log in [**AWS Console**](https://us-east-1.signin.aws.amazon.com/)

- On the **service** menu,click **EC2**.

- Click **Launch Instance**.

- In the navigation pane, choose **Quick Start**, choose **Ubuntu Server 16.04 LTS (HVM), SSD Volume Type**, and then click **Select**.

- On **Step2: Choose a Instance Type** page,make sure **t2.medium** is selected and click **Next: Configure Instance Details**.

- On **Step3: Configure Instance Details** page, keep all options default.

- Click **Next: Add Storage**, leave all values with their default.

- Click **Next: Tag Instance**.

- On **Step5: Tag Instance** page, enter the following information:
* Key: **Name**
* Value: **ROS Server**

- Click **Next: Configure Security Group**.

- On **Setp6: Configure Security Group** page, click **create a new security group**, enter the following information:
* Security group name: **ROSSG**
* Description: **ROS Server Security Group**

- Click **Add Rule**.

    - For Type, **Custom TCP**.
    - For Port Range, **5902**.
    - For Source, **Anywhere**.

<p align="">
    <img src="images/002-SGRule.png" width="90%" height="90%">
    </p>

- Click **Review and Launch**.

- Review the instance information and click **Launch**.

- Click **Create a new key pair**, enter the **Key pair name (ex.amazonec2_keypair_virginia)**, click **Download Key Pair**.

- Click **Launch Instances**.

- Scroll down and click **View Instances**.

- Wait for **ROS Server** shows 2/2 checks passed in the **Status Checks** column.

> This will take 3-5 minutes. Use the refresh icon at the top right to check for updates.

- On the Description tab in the lower panel, note the **Public IP** for the instance.

### Connect to EC2 Linux Instance via SSH

Here we'll use SSH until we need GUI. Open up your ec2 console to get the IP address.
<p align="">
    <img src="images/connect-to-ec2-01.png" width="90%" height="90%">
    </p>

<p align="">
    <img src="images/connect-to-ec2-02.png" width="60%" height="60%">
    </p>

(Optional) You may need to add **sudo** to grant pem file permission. For Example: 
```bash 
sudo ssh -i "YOUR_KEY.PEM" root@ec2-your-ip-address-here.compute-1.amazonaws.com
```

(Optional) If you are using Windows, you may also refer to [here](https://gitlab.com/ecloudture/olympic/connect-to-ec2-linux-instance).

### VNC Server Installation

<p align="">
    <img src="images/vnc.png" width="20%" height="20%">
</p>

VNC is a GUI remote control software which can be used to access AWS EC2. 

Install Packages.

```bash
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install ubuntu-desktop gnome-panel gnome-settings-daemon metacity nautilus gnome-terminal xfce4 vnc4server nano
```

After then, start the VNC Server.

```
vncserver
```

> Notice: It'll ask you to create a password. Enter it twice to verify. Remember it ,we will use it later.

Edit ~/.vnc/xstartup file.
```bash
nano ~/.vnc/xstartup
```

Paste all the contents with the lines below, then save it.

```bash
#!/bin/sh
# Uncomment the following two lines for normal desktop:
unset SESSION_MANAGER
# exec /etc/X11/xinit/xinitrc
unset DBUS_SESSION_BUS_ADDRESS
startxfce4 &
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid grey
vncconfig -iconic &
gnome-panel &
gnome-settings-daemon &
metacity &
nautilus &
gnome-terminal &
```
> Nano Hot Key <br>
> Paste: Ctrl + Shift + V (Windows : Right click)<br>
> Exit: Ctrl + X <br>
> Save: Y <br>
> Confirm: Enter

Stop the EC2 instance, and then start again.<br>
(We want to make sure the configuration has been updated.)

SSH into EC2 instance, and type as following 

```bash
vncserver
vncserver -geometry 1340x750
```


### Install VNC Viewer 
[Real VNC Download ](https://www.realvnc.com/en/connect/download/vnc/).

### Connect to EC2 with GUI 
Copy your **EC2** ip address and paste it to VNC Viewer. Add port number `:5902` at the end. Last, type enter to connect your **EC2**.

<p align="">
    <img src="images/001-ConnetToEC2.png" width="50%" height="50%">
</p>

Press **Continue**
<p align="">
    <img src="images/vnc-01.png" width="50%" height="50%">
</p>

The password is the one you set up with vncserver.
<p align="">
    <img src="images/vnc-02.png" width="50%" height="50%">
</p>
It should be like this.
<p align="">
    <img src="images/vnc-03.png" width="70%" height="70%">
</p>
(Optional) You can change picture quality here to load faster.
<p align="">
    <img src="images/vnc-04.png" width="50%" height="50%">
</p>


## ROS Installation
Now we finally can install **ROS** on **AWS EC2**. Please connect to **AWS EC2**. We don't need GUI during the installation process, so there's no need to use VNC now until TurtleSim Part. 


### Setup sources.list
Sources.list need to setup to provide download support. 
```bash 
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' 
```

### Set up keys
```bash 
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 
```

### Update apt-get packages
It is crucial to keep your apt-get related packages up to date before making any change or installation.

```bash
 sudo apt-get update 
```

### ROS Full Install
```bash
sudo apt-get install ros-kinetic-desktop-full
```

### Initialize rosdep
```bash
sudo rosdep init
rosdep update
```

### Environment setup
```bash
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

### Install Dependencies for building packages
```bash
sudo apt install python-rosinstall python-rosinstall-generator python-wstool build-essential
```

### Check ROS env path (Optional)
```bash
printenv | grep ROS 
```
<p align="">
    <img src="images/env-path.png" width="60%" height="60%">
    </p>

### Create a ROS Workspace
```bash
mkdir -p ~/catkin_ws/src 
cd ~/catkin_ws
catkin_make
source devel/setup.bash
```
<p align="">
    <img src="images/catkin-make.png" width="60%" height="60%">
    </p>

### Check ROS Package Path (Optional)
```bash
echo $ROS_PACKAGE_PATH
```
It should be `/home/ubuntu/catkin_ws/src:/opt/ros/kinetic/share `

### Install ros-tutorial Package
```bash 
cd ~/catkin_ws/src
git clone https://github.com/ros/ros_tutorials.git
cd ~/catkin_ws && catkin_make
```

## Run TurtleSim
We'll open a simualtion program, so GUI remote control is needed now. Use **VNC Viewer** to log in **AWS EC2**. 

### Open **Terminal**
<p align="">
    <img src="images/terminal.png" width="70%" height="70%" >
    </p>

### Start ROS Master

ROSCore is a collection of nodes and programs that are pre-requisites of a ROS-based system. You must have a ROSCore running in order for ROS nodes to communicate.

```bash
roscore
```

### Start TurtleSim Node
Open a new terminal then start TurtleSim simulation node.

```bash
rosrun turtlesim turtlesim_node
```
<p align="">
    <img src="images/turtlesim-02.png" width="40%" height="40%" >
    </p>

<p align="">
    <img src="images/turtlesim-01.png" width="40%" height="40%" >
    </p>

Open a new terminal then start teleop control node. Use arrow keys to control the turtle.
```bash
rosrun turtlesim turtle_teleop_key
```
<p align="">
    <img src="images/turtlesim-04.png" width="90%" height="90%">
    </p>
<p align="">
    <img src="images/turtlesim-03.png" width="40%" height="40%">
    </p>



