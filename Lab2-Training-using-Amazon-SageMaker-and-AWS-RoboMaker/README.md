# Training using Amazon SageMaker and AWS RoboMaker
## Overview
This lab will use Amazon SageMaker RL and AWS RoboMaker to simulate [AWS DeepRacer](https://console.aws.amazon.com/deepracer/home#welcome). When the cars learns to drive by interacting with its environment, taking an action in a given state to maximize the expected reward. Also learns the optimal plan of actions in training by trial-and-error through repeated episodes.

AWS RoboMaker is a service that makes it easy for developers to develop, test, and deploy robotics applications. 


<img src="./images/000-Overview.png"/>

## SageMaker Introduction
Amazon SageMaker provides every developer and data scientist with the ability to build, train, and deploy machine learning models quickly. Amazon SageMaker is a fully-managed service that covers the entire machine learning workflow to label and prepare your data, choose an algorithm, train the model, tune and optimize it for deployment, make predictions, and take action. Your models get to production faster with much less effort and lower cost.

Amazon SageMaker now enables developers and data scientists to quickly and easily develop reinforcement learning models at scale with Amazon SageMaker RL.

## RoboMaker Introduction
AWS RoboMaker is a service that makes it easy to develop, test, and deploy intelligent robotics applications at scale. RoboMaker extends the most widely used open-source robotics software framework, Robot Operating System (ROS), with connectivity to cloud services. This includes AWS machine learning services, monitoring services, and analytics services that enable a robot to stream data, navigate, communicate, comprehend, and learn. RoboMaker provides a robotics development environment for application development, a robotics simulation service to accelerate application testing, and a robotics fleet management service for remote application deployment, update, and management.

## Before We Start
- Make sure you are in __US East (N. Virginia)__, which short name is __us-east-1__.

## Upload CloudFormation Template to S3
- On the __Services__ menu, click __S3__, Click __Create bucket__.

<center><img src="./images/001-S3DashBoard.jpg"/></center>

- For Bucket Name, type `deepracer-lab2-yourname`.
> Please replace `yourname` by your name.
- For Region, choose __US East (N. Virginia)__, Click __Create__.

<center><img src="./images/002-CreateBucket.jpg" style="width:500px;"/></center>

- Select the bucket which you created before, and Upload [SageMakerForDeepRacerSetup.yaml](./SageMakerForDeepRacerSetup-new.yaml).

<center><img src="./images/003-UploadStack.png" style="width:500px;"/></center>

- Click __Upload__ to upload the stack to S3.

- Choose __SageMakerForDeepRacerSetup.yaml__ and copy the __Object URL__ to note.

<center><img src="./images/004-CopyS3Url.jpg" style="width:500px;"/></center>

## Create an IAM Role for CloudFormation

- On the __Services__ menu, click __IAM__.

- Select __Roles__ on the navigation panel.

- Select __Create role__.

- Select the items as following :

    - Select type of trusted entity : __AWS Services__

    - Choose the service that will use this role : __CloudForamtion__

    - Click __Next: Permissions__

<center><img src="./images/IAM_Role_1.jpg" style="width:500px;"/></center>

- Choose the following IAM policy to role :

    - __AmazonEC2FullAccess__

    - __IAMFullAccess__

    - __AWSDeepRacerCloudFormationAccessPolicy__
    
    - __AmazonSageMakerFullAccess__
    
- Click __Next: Tags__.

- (Optional) Add a tag to your role.

- Click __Next: Review__.

- Type __AWSDeepRacerCloudFormationAccessRole__ for role name.

- Check the additional policy was attached to the role, and the click __Create role__.

<center><img src="./images/IAM_Role_2.jpg" style="width:500px;"/></center>

## Create an Amazon SageMaker with CloudFormation
- On the __Services__ menu, click __CloudForamtion__.

- Select __Create Stack__ to create a CloudFormation template for SageMaker notebook instance.

- On the __Create stack__ page, in the __Prerequisite - Prepare template__ section, select __Template is ready__.

- In the __Specify template__ section, choose __Amazon S3 URL__  as __Template source__, and paste the __SageMakerForDeepRacerSetup.yaml object link__ that we copy from previous step in to __Amazon S3 URL__
 filed.

<center><img src="./images/005-CreateStack.jpg" style="width:500px;"/></center>

- On the __Specify stack details__ page, type `DeepRacer-Lab2` in the **Stack name**.

<center><img src="./images/006-CloudformationStackName.png" style="width:500px;"/></center>

- In the __Parameters__ section, use the default and click __Next__.

<center><img src="./images/007-CloudforamtionParameter1.png" style="width:500px;"/></center>
<center><img src="./images/008-CloudforamtionParameter2.png" style="width:500px;"/></center>

- On the __Permissions__ page, choose __AWSDeepRacerCloudFormationAccessRole__ as IAM Role, and Click __Next__.

<center><img src="./images/009-CloudformationIAM.png" style="width:500px;"/></center>

- Check all information and scroll down the page.

-  In the __Capabilities__ section, select __I acknowledge that AWS CloudFormation might create IAM resources.__ and click __Create stack__.
<center><img src="./images/010-CheckCloudformationDetail.png" style="width:500px;"/></center>

- When it completes, you will see the status as
__“CREATE_COMPLETE”__ in green, as shown in the __Stack Info__ tab below.
<center><img src="./images/011-CheckCloudFormationComplete.png" style="width:500px;"/></center>

- Switch to __Outputs__ tab and copy __SagemakerNotebook__ link and paste to browser.
<center><img src="./images/012-GetSagMakerNBUrl.jpg" style="width:500px;"/></center>

- In the SageMaker Notebook Instance Page, click __Open Jypter__.
<center><img src="./images/013-OpenJypterNB.png" style="width:500px;"/></center>

- You will see the page as below.
<center><img src="./images/014-JupyterOverview.png" style="width:500px;"/></center>

- Choose __DeepRacer-Lab2-Tutorial__ folder and open __DeepRacer_SageMaker_RoboMaker.ipynb__.

- If you see `Kernel not found`, please choose __conda_python3__.

<center><img src="./images/Set_Kernel.jpg" style="width:600px;"/></center>

- Click __Set Kernel__, and you are able to use the Jupyter notebook.

<center><img src="./images/015-NBOverview.png" style="width:600px;"/></center>

- Run cells in __Setting up the development environment__ section, you can use `shift`+`enter` to execute the cell.

- Execute **Creating the simulation environment**, **Launch the simulation job on AWS RoboMaker**, **Visualizing simulations in AWS RoboMaker** cells, and you will see a link when the Visualizing simulations in AWS RoboMaker cells is complete. 

<center><img src="./images/016-RoboMakerLink.png" style="width:600px;"/></center>

- Open the link in the new window, you will see the **AWS RoboMaker Console**.

    Make sure the Status of Robomaker is `running`

<center><img src="./images/Robomaker_Status.jpg" style="width:600px;"/></center>

- Choose __Gazebo__ as __Simulation tools__.

<center><img src="./images/017-RoboMakerGazebo.png" style="width:600px;"/></center>

- The visualization of simulation jobs will show as below.
<center><img src="./images/018-VisualizationSimulationJobs.png" style="width:600px;"/></center>

- Now you can go back to the Jupyter notebook, executing __Plot metrics for training job__ cell.
<center><img src="./images/019-PlotMetrics.png" style="width:600px;"/></center>

- Execute the __Clean up training resources__  to clean up Amazon SageMaker training instance and the AWS RoboMaker simulation job.

- Run the __Evaluation__ cell to evaluate jobs. The point of an evaluation is to help you assess the model's behaviors in a controlled simulation.

- Execute the __Clean up simulation application resource__  to shut down AWS RoboMaker evaluation simulation app.

## Conclusion
Congratulations! You now have successfully:
- Set up the appropriate environment and install the right packages to take advantage of Amazon SageMaker.  
- Build, train, and evaluate an enhanced learning model for AWS DeepRacer using Amazon SageMaker. 
- Create, launch, and visualize simulation environments in AWS RoboMaker.

## Clean Up 
- On the __Services__ menu, click __CloudForamtion__.
- Select stack that named `DeepRacer-Lab2` and click **Delete** to delete the stack.
<center><img src="./images/020-CleanUpCloudFormation.png" style="width:600px;"/></center>

- Choose __Delete stack__ when you see the confirm windows.
<center><img src="./images/021-DeleteStack.png" style="width:400px;"/></center>
