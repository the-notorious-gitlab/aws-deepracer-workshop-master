# Deploy and tuning model with DeepRacer

![image](images/001-DeepRacerLogo.png)

The AWS DeepRacer console is a graphical user interface to integrate with DeepRacer Service. DeepRacer console provides you with the feature of training reinforcement learning model and evaluating the model performance. You can also download a trained model for deployment to physical environment. Therefore, you do not need to manually set up a software environment, simulator or training environment.

## Before We Start

Sign in your AWS Account and enter **AWS DeepRacer Service**.

## Create Reinforment Learning Model

Now we'll set up a reinforcement learning model and start training.

### Get Started

- On AWS DeepRacer navigation, choose **Reinforcement learning**.

- Choose **Create model**.

### Account Resources

Choose **Create resources**, if this is your first time to use AWS DeepRacer. It'll take about 5 minutes.

<p align="">
    <img src="images/003-AccountResources.png" width="70%" height="40%">
</p>

<!-- 解釋資源用途 -->
> **S3 Bucket:** Store model and training logs. <br>
> **RoboMaker simulation:** Provide ROS environment to run the DeepRacer simulation. <br>
> **AWS SageMaker:** Provide training environment to train model using reinforcement learning algorithm.


### Model Details

Enter a model name and optionally prvide a brief description in **Model description - optional** input field.

<p align="">
    <img src="images/004-ModelDetails.png" width="70%" height="40%">
</p>

### Environment Simulation

Choose the track to train your model. We select the **re:Invent 2018**, you can also choose other tracks.

<p align="">
    <img src="images/005-EnvironmentSimulation.png" width="70%" height="40%">
</p>

### Action Space

For the first time, you can set all parameters default. In order to reduce training time and increase the speed of car, we recommend a **Maximum speed** at least `5`.

<p align="">
    <img src="images/006-ActionSpace.png" width="70%" height="40%">
</p>

### Reward function

For the first time, you can choose default reward function **Follow the center line** or other examples. You may also write your own reward function.

<p align="">
    <img src="images/007-RewardFunction.png" width="70%" height="50%">
</p>

### HyperParameters

Specify hyperparameters to tune your training performance or keep it default.

<!-- <p align="">
    <img src="images/008-Hyperparameters-1.png" width="60%" height="50%">
</p> -->

#### Batch size

<p align="">
    <img src="images/013-BatchSize.png" width="30%" height="50%">
</p>

> **Bacth size:** The batch is a subset of an experience buffer that is composed of images captured by the camera mounted on the AWS DeepRacer vehicle and actions taken by the vehicle.

#### Number of epoches

<p align="">
    <img src="images/014-NumberOfEpoches.png" width="20%" height="50%">
</p>

> **Number of epochs:** The number of passes through the training data to update the neural network weights during gradient descent. Use a larger number of epochs to promote more stable updates, but expect a slower training. When the batch size is small, you can use a smaller number of epochs.

#### Learning rate

<p align="">
    <img src="images/015-LearningRate.png" width="40%" height="50%">
</p>

> **Learning rate:** The learning rate controls how much a gradient-descent (or ascent) update contributes to the network weights. Use a higher learning rate to include more gradient-descent contributions for faster training, but be aware of the possibility that the expected reward may not converge if the learning rate is too large.

#### Entropy

<p align="">
    <img src="images/016-Entropy.png" width="25%" height="50%">
</p>

> **Entropy:** The degree of uncertainty used to determine when to add randomness to the policy distribution. The added uncertainty helps the AWS DeepRacer vehicle explore the action space more broadly. A larger entropy value encourages the vehicle to explore the action space more thoroughly.

#### Discount factor

<p align="">
    <img src="images/017-DiscountFactor.png" width="20%" height="50%">
</p>

> **Discount factor:** The discount factor determines how much of future rewards are discounted in calculating the reward at a given state as the averaged reward over all the future states.With the discount factor of 0.9, the expected reward at a given step includes rewards from an order of 10 future steps. With the discount factor of 0.999, the expected reward includes rewards from an order of 1000 future steps.

#### Loss type

<p align="">
    <img src="images/018-LossType.png" width="20%" height="50%">
</p>

> **Loss type:** The type of the objective function to update the network weights.The Huber and Mean squared error loss types behave similarly for small updates.But as the updates become larger, the Huber loss takes smaller increments compared to the Mean squared error loss. When you have convergence problems, use the Huber loss type. When convergence is good and you want to train faster, use the Mean squared error loss type.

#### Number of experience

<p align="">
    <img src="images/019-NumberOfExperience.png" width="50%" height="50%">
</p>

> **Number of experience:** The size of the experience buffer used to draw training data from for learning policy network weights. Different episodes can have different lengths. For simple reinforcement-learning problems, a small experience buffer may be sufficient and learning will be fast. For more complex problems which have more local maxima, a larger experience buffer is necessary to provide more uncorrelated data points. In this case, training will be slower but more stable. The recommended values are 10, 20 and 40.

### Stop Conditions

Below **Stop conditions**, set a Maximum time value to terminate long-running training session. You may adjust it yourself.

<p align="">
    <img src="images/009-StopConditions.png" width="60%" height="50%">
</p>

### Start Training

Choose button **Start training** to start training job.  It will take about 6 minutes before your training starts.

## Training in progress

Choose the model you created and select the **Training** tab. Reward growth can be observed through **Reward graph**.

<p align="">
    <img src="images/010-RewardGraph.png" width="70%" height="50%">
</p>

## Evaluation

When training is completed, let's start to evaluate the  model performance.

### First Evaluation

Just choose button **Start evaluation**, evaluation job will be started. After a few minutes, the result will be displayed as below.

<p align="">
    <img src="images/011-EvaluationResult.png" width="70%" height="50%">
</p>

## Deploy model to AWS DeepRacer

Now, you will download model you trained and delpoy it to physical DeepRacer.

### Download model

- In model list page, choose model you want to deploy.

- Choose button **Download model**.

- Please wait a few minute, model will start downloading.

<p align="">
    <img src="images/012-DownloadModel.png" width="70%" height="50%">
</p>

- Model is a file, the end of file name is `.tar.gz`.

- Now, just copy your model file to USB and insert it to DeepRacer!

## Clean up

- Enter **AWS CloudFormation** console.

- Choose the stack starting with **deepracer-vpc...** and press the button **Delete**.

<p align="">
    <img src="images/020-CleanUp.png" width="70%" height="50%">
</p>